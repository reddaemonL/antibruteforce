package postgres

import (
	"context"
	"database/sql"

	"github.com/jmoiron/sqlx"
	"go.uber.org/zap"
)

const Blacklist string = "blacklist"
const Whitelist string = "whitelist"

// Repository interface contain methods to work with storage
type Repository interface {
	AddToBlacklist(ctx context.Context, subnet string) error
	RemoveFromBlacklist(ctx context.Context, subnet string) error
	AddToWhitelist(ctx context.Context, subnet string) error
	RemoveFromWhitelist(ctx context.Context, subnet string) error
	FindIPBlacklist(ctx context.Context, ip string) (string, error)
	FindIPWhitelist(ctx context.Context, ip string) (string, error)
}

type PsqlRepository struct {
	*sqlx.DB //nolint
	logger   *zap.Logger
}

func NewPsqlRepository(DB *sqlx.DB, logger *zap.Logger) *PsqlRepository { // nolint
	return &PsqlRepository{DB: DB, logger: logger}
}

func (p PsqlRepository) AddToBlacklist(ctx context.Context, subnet string) error {
	query := `INSERT INTO blacklist (subnet) VALUES ($1)`
	_, err := p.DB.ExecContext(ctx, query, subnet)

	return err
}

func (p PsqlRepository) RemoveFromBlacklist(ctx context.Context, subnet string) error {
	query := `DELETE FROM blacklist WHERE subnet = $1`
	_, err := p.DB.ExecContext(ctx, query, subnet)

	return err
}

func (p PsqlRepository) AddToWhitelist(ctx context.Context, subnet string) error {
	query := `INSERT INTO whitelist (subnet) VALUES ($1)`
	_, err := p.DB.ExecContext(ctx, query, subnet)

	return err
}

func (p PsqlRepository) RemoveFromWhitelist(ctx context.Context, subnet string) error {
	query := `DELETE FROM whitelist WHERE subnet = $1`
	_, err := p.DB.ExecContext(ctx, query, subnet)

	return err
}

func (p PsqlRepository) FindIPBlacklist(ctx context.Context, ip string) (string, error) {
	query := `
		SELECT distinct $2 as list FROM blacklist where $1::inet <<= subnet
	`
	list := make([]string, 0, 2)

	err := p.DB.SelectContext(ctx, &list, query, ip, Blacklist)

	if err != nil && err != sql.ErrNoRows {
		return "", err
	}

	switch len(list) {
	case 0: // nolint
		return "", nil
	case 1: // nolint
		return list[0], nil
	default:
		return Blacklist, nil
	}
}

func (p PsqlRepository) FindIPWhitelist(ctx context.Context, ip string) (string, error) {
	query := `
		SELECT distinct $2 as list FROM whitelist where $1::inet <<= subnet
	`
	list := make([]string, 0, 2)

	err := p.DB.SelectContext(ctx, &list, query, ip, Whitelist)

	if err != nil && err != sql.ErrNoRows {
		return "", err
	}

	switch len(list) {
	case 0: // nolint
		return "", nil
	case 1: // nolint
		return list[0], nil
	default:
		return Whitelist, nil
	}
}
