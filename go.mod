module gitlab.com/reddaemonL/antibruteforce

go 1.13

require (
	github.com/cockroachdb/apd v1.1.0 // indirect
	github.com/cucumber/godog v0.10.0
	github.com/golang/protobuf v1.3.3
	github.com/grpc-ecosystem/go-grpc-middleware v1.2.0
	github.com/jackc/fake v0.0.0-20150926172116-812a484cc733 // indirect
	github.com/jackc/pgx v3.6.2+incompatible
	github.com/jmoiron/sqlx v1.2.0
	github.com/lib/pq v1.3.0
	github.com/shopspring/decimal v0.0.0-20200105231215-408a2507e114 // indirect
	github.com/spf13/cobra v0.0.6
	github.com/spf13/viper v1.6.2
	go.uber.org/zap v1.14.0
	golang.org/x/net v0.0.0-20200222125558-5a598a2470a0
	golang.org/x/sync v0.0.0-20190911185100-cd5d95a43a6e
	golang.org/x/sys v0.0.0-20200302150141-5c8b2ff67527 // indirect
	google.golang.org/grpc v1.27.1
)
