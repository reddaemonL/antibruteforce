package cmd

import (
	"log"
	"time"

	"github.com/spf13/cobra"
	api "gitlab.com/reddaemonL/antibruteforce/protofiles"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

func init() { // nolint
	auth.PersistentFlags().StringVarP(&address, "address", "a", "localhost:8080", "server address")
	auth.PersistentFlags().StringVarP(&login, "login", "l", "", "login to auth")
	auth.PersistentFlags().StringVarP(&password, "password", "p", "", "password to auth")
	auth.PersistentFlags().StringVarP(&ip, "ip", "i", "", "ip to auth")
	rootCmd.AddCommand(auth)
}

var auth = &cobra.Command{ //nolint
	Use:   "auth",
	Short: "Auth",
	Long:  "Auth",
	Run: func(cmd *cobra.Command, args []string) {
		conn, err := grpc.Dial(address, grpc.WithInsecure(), grpc.WithBlock())
		if err != nil {
			log.Fatalf("unable to connect: %v", err)
		}
		defer conn.Close()
		c := api.NewAntiBruteforceClient(conn)
		ctx, cancel := context.WithTimeout(context.Background(), time.Second)
		defer cancel()
		r, err := c.Auth(ctx, &api.AuthRequest{Login: login, Ip: ip, Password: password})
		if err != nil {
			log.Fatalf("unable to auth with: %v", err)
		}
		log.Printf("Done: %t", r.Ok)
	},
}
