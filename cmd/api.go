package cmd

import (
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"
	"runtime"

	"gitlab.com/reddaemonL/antibruteforce/internal/service/api/server/middleware"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	"github.com/spf13/cobra"
	"gitlab.com/reddaemonL/antibruteforce/config"
	dbInstance "gitlab.com/reddaemonL/antibruteforce/db"
	"gitlab.com/reddaemonL/antibruteforce/internal/database/postgres"
	"gitlab.com/reddaemonL/antibruteforce/internal/service/api/bucket"
	"gitlab.com/reddaemonL/antibruteforce/internal/service/api/server"
	"gitlab.com/reddaemonL/antibruteforce/internal/service/api/usage"
	"gitlab.com/reddaemonL/antibruteforce/logger"
	grpcapi "gitlab.com/reddaemonL/antibruteforce/protofiles"
)

// apiCmd represents the api command
var apiCmd = &cobra.Command{ // nolint
	Use:   "api",
	Short: "start api",
	Long:  `start api`,
	Run: func(cmd *cobra.Command, args []string) {
		runtime.SetBlockProfileRate(1)
		configPath := flag.String("config", "config.yml", "path to config file")
		flag.Parse()
		c, err := config.GetConfig(*configPath)
		if err != nil {
			log.Fatalf("unabler to get config: %v", err)

		}
		l, err := logger.GetLogger(c)
		if err != nil {
			log.Fatalf("unable to get logger: %v", err)
		}

		db, err := dbInstance.GetDB(c)
		if err != nil {
			log.Fatalf("unable to get db: %v", err)
		}
		lis, err := net.Listen("tcp", c.URL)
		if err != nil {
			l.Fatal(fmt.Sprintf("failed to listen %v", err))
		}
		l.Info("server started at " + c.URL)
		// gRPC server statup options
		var opts []grpc.ServerOption
		opts = middleware.AddLogging(l, opts)
		grpcServer := grpc.NewServer(opts...)

		if c.IsDev() {
			reflection.Register(grpcServer)
		}

		r := postgres.NewPsqlRepository(db, l)
		br := bucket.NewMemRepo(l)
		u := usage.NewUsage(r, br, l, c)

		// add middleware

		grpcapi.RegisterAntiBruteforceServer(grpcServer, server.NewServer(u, l))

		go func() {
			err := http.ListenAndServe(":8024", nil)
			if err != nil {
				log.Fatal("Cannot run http server..")
			}
		}()

		err = grpcServer.Serve(lis)
		if err != nil {
			l.Fatal(err.Error())
		}
	},
}

func init() { // nolint
	rootCmd.AddCommand(apiCmd)
}
