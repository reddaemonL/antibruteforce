package db

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/reddaemonL/antibruteforce/config"
)

func GetDB(c *config.Config) (*sqlx.DB, error) {
	psqlInfo := fmt.Sprintf("host=%s port=%s user=%s "+
		"password=%s dbname=%s sslmode=disable",
		c.DB["host"], c.DB["port"], c.DB["user"], c.DB["pass"], c.DB["name"])

	return sqlx.Connect("postgres", psqlInfo)
}
