package tests

import (
	"os"
	"testing"

	"github.com/cucumber/godog"
)

//var opt = godog.Options{
//	Output: colors.Colored(os.Stdout),
//	Format: "progress", // can define default values
//}

func TestMain(m *testing.M) {
	status := godog.RunWithOptions("godog", func(s *godog.Suite) {
		FeatureContext(s)
	}, godog.Options{
		Format:    "progress",
		Paths:     []string{"features"},
		Randomize: 0,
	})

	if st := m.Run(); st > status {
		status = st
	}

	os.Exit(status)
}
